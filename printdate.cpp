#include <iostream>
#include <string>
#include <regex>

using namespace std;
int main()
{

    string date_input;
    cout << "enter the date(YYYY-MM-DD, MM/DD/YYYY, DD.MM.YYYY, DD-MM-YYYY) : ";

    getline(cin, date_input);
    regex yyyy_mm_dd_regex("\\d{4}_\\d{2}_\\d{2}");
    regex mm_dd_yyyy_regex("\\d{2}/\\d{2}/\\d{4}");
    regex dd_mm_yyyy_dot_regex("\\d{2}.\\d{2}.\\d{4}");
    regex dd_mm_yyyy_dash_regex("\\d{2}-\\d{2}-\\d{4}");
    smatch match;
    if (regex_match(date_input, match, yyyy_mm_dd_regex))
    {
        string year_str = date_input.substr(0, 4);
        string month_str = date_input.substr(5, 2);
        string day_str = date_input.substr(8, 2);
        int year = stoi(year_str);
        int month = stoi(month_str);
        int day = stoi(day_str);
        cout << "Year: " << year << ", Month: " << month << ", Day: " << day << endl;
    }
    else if (regex_match(date_input, match, mm_dd_yyyy_regex))
    {
        string year_str = match.str(0).substr(6, 4);
        string month_str = match.str(0).substr(0, 2);
        string day_str = match.str(0).substr(3, 2);
        int year = stoi(year_str);
        int month = stoi(month_str);
        int day = stoi(day_str);
        cout << "Year: " << year << ", Month: " << month << ", Day: " << day << endl;
    }
    else if (regex_match(date_input, match, dd_mm_yyyy_dot_regex))
    {
        string year_str = match.str(0).substr(6, 4);
        string month_str = match.str(0).substr(3, 2);
        string day_str = match.str(0).substr(0, 2);
        int year = stoi(year_str);
        int month = stoi(month_str);
        int day = stoi(day_str);
        cout << "Year: " << year << ", Month: " << month << ", Day: " << day << endl;
    }
    else if (regex_match(date_input, match, dd_mm_yyyy_dash_regex))
    {
        string year_str = match.str(0).substr(6, 4);
        string month_str = match.str(0).substr(3, 2);
        string day_str = match.str(0).substr(0, 2);
        int year = stoi(year_str);
        int month = stoi(month_str);
        int day = stoi(day_str);
        cout << "Year: " << year << ", Month: " << month << ", Day: " << day << endl;
    }
    else
    {
        cout << "invalid format: please select from given format";
    }

    return 0;
}